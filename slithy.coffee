# Slithy: slideshow hack.

# Usual jQuery.ready wrapper: wait to ensure DOM is… ready.
(($)->
	# On demand: only run if post actually contains slides — tagged "Slides".
	unless ($ '.post.tag-slides').length then return # These are not the droids you are looking for.

	## Hack DOM into (quasi) compliance with S5 microformat.
	# 1. Wrap slides in <section>s, so can show/hide individually.
	$ '.entry-content>h2'
	.each (i,e)->
		e=$ e # (Always) prefer jQuery's over DOM API.
		e
		.nextUntil 'h2' # (Or EOF.)
		.addBack() # Include the preceding <h2>.
		.wrapAll '<section>'
		# Move <h2>'s attributes to <section>.
		for a in ['id','data-parent']
			do (a)->
				e.parent().attr a,(e.attr a) ? null # Cf https://devdocs.io/jquery/attr .
				e.removeAttr a
	# 2. Class container and slides: .presentation>.slide.
	$ '.entry-content'
	.addClass 'presentation'
	.children 'section'
	.addClass 'slide'
	# 3. Insert divs for dynamic controls, etc: .layout>#controls,#currentSlide,#header (but not #footer — collides with WP… or theme?!).
	$ '<div class="layout"><div id="controls"></div><div id="currentSlide"></div><div id="header"></div></div>' #? #footer?
	.insertAfter $ 'h1.entry-title'
	#?… .slide>h2→h1?

	# "Globals".
	slides=$ 'section.slide'

	# Generate missing [id]s.
	missing=slides.not '[id]' # Who wants an ID?
	headings=missing
	.children 'h2'
	.map (i,e)->($ e).text().toLowerCase() # (Hope jQuery#attr escapes everything?)
	.get() # Plain JS array.
	# (Private utility function:) (length of) longest identical prefix between two strings.
	prefix=(x,y,i=0)->if i>x.length or i>y.length or x[i] isnt y[i] then i else prefix x,y,i+1
	# Set each [id] to shortest prefix that doesn't match any preceding title. If possible.
	missing
	.each (i,e)->
		try
			# (Array of lengths of) shortest unique abbreviations.
			shorts=for t in headings[...i]
				if t is headings[i] then console.error 'ERROR: identical headings, can\'t generate missing [id]s.'; throw undefined # Won't deal with identical headings.
				1+prefix t,headings[i] # +1 guarantees difference.
			$ e
			.attr 'id',headings[i].slice 0,Math.max ([1].concat shorts)...

	# Change slide.
	# @param x: selector (ID, string) or index (number).
	slide=(x)->
		if x is '' or x is '#' then x=-1 # When no URL fragment, location.hash is empty string.
		i=if typeof x is 'number' then x else slides.index $ x #? Also find ID inside slides?
		unless 0<=i<slides.length then i=0 # Default to first when not found or out of range.
		# Toggle current.
		slides
		.removeClass 'current'
		.eq i
		.addClass 'current'
		# Show progress.
		($ '#currentSlide').text "#{i+1}/#{slides.length}" # One-based index. ;o)

	# Change slide when location.hash changes — hyperlink jumps.
	$ window
	.on 'popstate',->slide location.hash

	# Button to toggle between slideshow mode and regular post.
	$ '<button title="Slideshow" accesskey="s">📽</button>' #? Replace glyph with SVG!
	.insertAfter $ 'h1.entry-title'
	.click (ev)->
		($ ev.target).blur() # Lose focus, so pressing space now doesn't click it.
		($ 'body').toggleClass 'slideshow'
		if ($ 'body.slideshow').length # Is view now in slideshow mode?
			slide location.hash # Show slide: indicated in URL, otherwise first.

	# Navigate.
	# @param x: [id] of slide as string, or slide as (wrapped by) jQuery object.
	go=(x)->location.hash=if typeof x isnt 'object' then x ? '' else x.attr 'id'

	# Keyboard shortcuts.
	$ document
	.keydown (ev)->
		unless ($ 'body.slideshow').length then return # Only handle keys when in slideshow mode.
		switch ev.keyCode
			when 27 # Esc: quit slideshow mode.
				$ 'body'
				.removeClass 'slideshow'
			when 32 # Space
				if ev.shiftKey #? Ugly duplication!
					go ($ '.current').prev()
				else
					go ($ '.current').next()
			when 34,39 # PgDn, Right: next.
				go ($ '.current').next()
			when 33,38 # PgUp, Up: previous.
				go ($ '.current').prev()
			when 37 # Left: up to parent.
				go ($ '.current').data 'parent'
			when 40 # Down: skip to next sibling — if any.
				p=($ '.current').data 'parent'
				if p
					n=$ ".current~[data-parent=#{p}]" # Find next siblings — same parent.
					if n.length then go n
					else go ($ '.current').next() #? What to do if last?!
				else
					go $ '.current~:not([data-parent])' # Find next siblings — no parent.
			when 35 # End: last.
				go slides.last()
			when 36 # Home: first.
				go slides.first()

# IIFE to ensure "$" aliases jQuery — at least locally.
)(jQuery)
