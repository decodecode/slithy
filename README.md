# Slithy
> Slideshow hack for my WordPress blog.

[Read all about it!](https://decodecode.net/elitist/2019/07/slithy/)

[Code](https://gitlab.com/decodecode/slithy) on GitLab.

## TL;DR
…

## Features

- Dynamically patches markup to (kindda) comply with XOXO/S5 microformat
- But doesn't disrupt post's normal markup and styling
- On demand: only runs in posts tagged "slides"

## Installation

1. Build

	```
	$ coffee --output dist --compile slithy.coffee
	$ stylus --out dist --compress slithy.styl
	```

1. Deploy

	```
	$ scp dist/slithy.{js,css} $ACCOUNT:$BLOG/wp-content/themes/stencil/js
	```

	(Directory "js" must exist already.)

	Or copy manually — in file manager, open tab:

	```
	sftp://$ACCOUNT/$BLOG/wp-content/themes/stencil/js/
	```

## Release notes

### Next, currently in development
…

### 0.9.1, released 2019-07-21

- Styling fix: override Stencil's.

### 0.9.0, released 2019-07-21

- Navigation, keyboard shortcuts, progress, missing [id]s generated, styling, URLs… It works!
- Navigate "down", skip children: probably broken; under-specified?
- Dropped (need for) `<ol>` wrapper, and dynamically wrapping slides in `<section>`s, to replace `<li>`s.
- Split from TOC hack, and from Stencil (WP theme)

### Draft, deployed 2019-06-30

- TOC: wrapped menu items in details-summary — HTML's built-in collapser.

### Draft, deployed 2019-06-17

- Buttons hidden, but TOC's accelerator (Shift+Alt+T) works — on both FF and ancient Chromium.
- Markup "enriched" to comply with S5 microformat.
